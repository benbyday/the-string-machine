import model.machinery.RuleBook
import model.machinery.SimpleRule
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import kotlin.test.*

object RuleBookSpec : Spek({
    describe("a rule book") {
        var book = RuleBook("the book")
        val emptyRule = SimpleRule("", """\s\w\s""", " * ")

        val findNames = listOf("digit", "alpha", "white space", "vowel", "group")
        val findList = listOf("""\d""", """[A-z]""", """\s""", """[aeiou]""", """#(\w+)""")
        val replaceNames = listOf("star", "empty")
        val replaceList = listOf("*", "")
        val ruleList = replaceNames.zip(replaceList).map {
            val replaceName = it.first
            val replace = it.second
            findNames.zip(findList).map {
                val findName = it.first
                val find = it.second
                SimpleRule("$findName $replaceName", find, replace)
            }
        }.flatten()

        fun fillBook() {
            for (rule in ruleList) {
                book.add(rule)
            }
        }

        fun utilizeNames(test: (String) -> Unit) {
            for (replace in replaceNames) {
                for (find in findNames) {
                    test("$find $replace")
                }
            }
        }

        fun utilizeFindRep(test: (String, String) -> Unit) {
            for (replace in replaceList) {
                for (find in findList) {
                    test(find, replace)
                }
            }
        }

        context("constructor") {
            it("constructs using any ruleName") {
                assertNotNull(RuleBook("foo"))
                assertNotNull(RuleBook("bar"))
                assertNotNull(RuleBook(""))
            }
        }
        context("add") {
            afterEachTest {
                book = RuleBook("the book")
            }

            it("always returns true for the first rule") {
                for (rule in ruleList) {
                    val tempBook = RuleBook("temp")
                    assertNotNull(tempBook.add(rule))
                }
            }
            it("will return true when adding a list of different rules") {
                for (rule in ruleList) {
                    assertNotNull(book.add(rule))
                }
            }
            context("full book") {
                beforeEachTest {
                    fillBook()
                }

                it("returns false when a rule with the same ruleName is added") {
                    utilizeNames { name: String ->
                        val dbl = SimpleRule(name, "foo", "bar")
                        assertNull(book.add(dbl))
                    }
                }
                it("returns false when a rule with the same definition is added") {
                    utilizeFindRep { find, replace ->
                        val dbl = SimpleRule("ruleName", find, replace)
                        assertNull(book.add(dbl))
                    }
                }
            }
        }
        context("get") {
            context("empty book") {
                it("returns null") {
                    utilizeNames {
                        assertNull(book["foo"])
                        assertNull(book["bar"])
                        assertNull(book[""])
                        assertNull(book[it])
                    }
                }
                it("grabs a rule with an empty ruleName") {
                    book.add(emptyRule)
                    val grabbed = book[""]
                    assertNotNull(grabbed)
                    assertEquals(emptyRule, grabbed)
                    assertNull(book["foobar"])
                    utilizeNames {
                        assertNull(book[it])
                    }
                }
            }
            context("full book") {
                beforeEachTest {
                    fillBook()
                }

                afterEachTest {
                    book = RuleBook("the book")
                }

                it("returns rules by ruleName") {
                    for (rule in ruleList) {
                        val name = rule.name
                        val copy = rule.copy()
                        val grabbed = book[name]
                        copy.name = "foobar"
                        assertNotNull(grabbed)
                        assertEquals(rule, grabbed)
                        assertEquals(grabbed, copy)
                    }
                }
                it("returns null for unknown names") {
                    assertNull(book["foobar"])
                    assertNull(book[""])
                    utilizeNames {
                        val name = "$it*"
                        assertNull(book[name])
                    }
                }
                it("grabs empty ruleName rules") {
                    book.add(emptyRule)
                    val grabbed = book[""]
                    assertNotNull(grabbed)
                    assertEquals(emptyRule, grabbed)
                    assertNull(book["foobar"])
                    utilizeNames {
                        val name = it + "12345"
                        assertNull(book[name])
                    }
                }
            }
        }
        context("find rule ruleName") {
            context("empty book") {
                it("returns null for any rule") {
                    for (rule in ruleList) {
                        assertNull(book.findRuleName(rule))
                    }
                    assertNull((book.findRuleName(emptyRule)))
                }
            }
            context("full book") {
                beforeEachTest {
                    fillBook()
                }

                afterEachTest {
                    book = RuleBook("the book")
                }

                it("returns the ruleName of the rule searched for") {
                    for (rule in ruleList) {
                        assertEquals(rule.name, book.findRuleName(rule))
                    }
                    utilizeNames {
                        assertEquals(it, book.findRuleName(book[it] ?: emptyRule))
                    }
                }
                it("returns null for any rules not in the book") {
                    assertNull(book.findRuleName(emptyRule))
                }
                it("mismatches rules by a different ruleName") {
                    for (rule in ruleList) {
                        val copy = rule.copy()
                        copy.name = rule.name + "12345"
                        assertNotEquals(copy.name, book.findRuleName(copy))
                    }
                }
            }
        }
    }
})
import model.machinery.Punchcard
import model.machinery.RuleBook
import model.machinery.SMOffice
import model.staff.Librarian
import model.staff.Secretary
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

object SecretarySpec : Spek({
    describe("A secretary") {
        val sam = Secretary
        val fileName = "C:\\Users\\ben.polson\\Documents\\Scripts\\kotlin\\string-machine\\src\\test\\resources" +
                "\\office_files\\rule_books\\cosmetic.csv"
        val bookDirName = "C:\\Users\\ben.polson\\Documents\\Scripts\\kotlin\\string-machine\\src\\test\\resources" +
                "\\office_files\\rule_books"
        val punchcardName = "C:\\Users\\ben.polson\\Documents\\Scripts\\kotlin\\string-machine\\src\\test\\resources" +
                "\\office_files\\punchcards\\cp_to_orion.csv"
        val folderName = "C:\\Users\\ben.polson\\Documents\\Scripts\\kotlin\\string-machine\\src\\test\\resources" +
                "\\office_files\\punchcards"
        context("creating a RuleBook from a file") {
            val book = sam.createBook(fileName)
            it("successfully returns a rulebook with a good file") {
                assertTrue {
                    book is RuleBook
                }
            }
            it("creates the correct number of rules") {
                assertEquals(12, book?.size())
            }
        }
        context("building the library from scratch") {
            var created = false
            beforeGroup { created = sam.createLibrary(bookDirName) }
            it("Successfully builds the library") {
                assertTrue {
                    created
                }
            }
            it("creates the correct number of books and rules") {
                assertEquals(3, SMOffice.ruleLibrary.size)
                assertEquals(12, SMOffice.getBook("cosmetic")?.size())
                assertEquals(17, SMOffice.getBook("cp")?.size())
                assertEquals(3, SMOffice.getBook("analytics")?.size())
            }
            group("making a punchcard from a file after making a library") {
                var punchcard: Punchcard? = null
                beforeGroup { punchcard = sam.createPunchcard(punchcardName) }
                it("Successfully makes a punchcard") {
                    assertTrue {
                        punchcard is Punchcard
                    }
                }
                it("punchcard is not broken") {
                    assertFalse {
                        punchcard!!.broken
                    }
                }
                it("punchcard has correct number of rules") {
                    assertEquals(29, punchcard?.size())
                }
            }
            group("create punchcard folder after making a library") {
                it("successfully makes the folder") {
                    assertTrue {
                        sam.createFolder(folderName)
                    }
                }
                it("creates the correct number of punchcards and rules on each punchcard") {
                    assertEquals(2, SMOffice.punchcardFolder.size)
                    assertEquals(29, SMOffice.getPunchcard("cp to orion")?.size())
                    assertEquals(2, SMOffice.getPunchcard("orion to analytics")?.size())
                }
            }
        }
    }
})
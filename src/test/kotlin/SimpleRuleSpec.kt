import model.machinery.SimpleRule
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import kotlin.test.*

object SimpleRuleSpec: Spek({
    describe("a simple rule") {
        val findSample = listOf<String>("""\d""", """[A-z]""", """\s""", """[aeiou]""", """#(\w+)""")
        val inputExample = "#a1 #b2 #c3 #d4 #e5 #aeiou"

        fun starRule(stringRule: String): SimpleRule {
            return SimpleRule(stringRule, "*")
        }
        fun emptyRule(stringRule: String): SimpleRule {
            return SimpleRule(stringRule, "")
        }

        context("constructor") {
            it("fails when find is an empty Regex") {
                assertFails {
                    SimpleRule("", "")
                }
                assertFails {
                    SimpleRule("", "", "")
                }
                assertFails {
                    SimpleRule("ruleName","","")
                }
                assertFails {
                    SimpleRule("","replace")
                }
                assertFails {
                    SimpleRule("foo", "", "bar")
                }
            }
            it("does not break when find is not empty") {
                for(find in findSample) {
                    assertNotNull(emptyRule(find))
                    assertNotNull(starRule(find))
                    assertNotNull(SimpleRule("ruleName",find,""))
                    assertNotNull(SimpleRule("foo",find,"bar"))
                }
            }
        }
        context("implement") {
            it("does nothing with an empty string") {
                for(find in findSample) {
                    val emp = starRule(find).implement("")
                    assertEquals("", emp)
                }
            }
            it("replaces all instances globally") {
                for(find in findSample) {
                    val stars = starRule(find).implement(inputExample)
                    assertTrue {
                        stars.count {
                            it == '*'
                        } >= 5
                    }
                }
            }
            it("makes strings shorter with an empty replace") {
                for (find in findSample) {
                    val emptied = emptyRule(find).implement(inputExample)
                    assertTrue {
                        emptied.length < inputExample.length
                    }
                }
            }
            it("keeps match data") {
                val inputResult = "*a1* *b2* *c3* *d4* *e5* *aeiou*"
                assertEquals(inputResult,
                        SimpleRule("""#(\w+)""", """*$1*""").implement(inputExample))
            }
        }
        context("equals") {
            it("is equal to itself") {
                for (find in findSample) {
                    val star = starRule(find)
                    val empt = emptyRule(find)
                    assertEquals(star, star)
                    assertEquals(empt, empt)
                }
            }
            it("is equal to a copy of itself") {
                for (find in findSample) {
                    assertEquals(starRule(find), starRule(find))
                    assertEquals(emptyRule(find), emptyRule(find))
                }
            }
            it("is equal to a copy with a different ruleName") {
                for (find in findSample) {
                    val empty1 = emptyRule(find)
                    val empty2 = emptyRule(find)
                    val star1 = starRule(find)
                    val star2 = starRule(find)
                    empty1.name = "empty foo"
                    empty2.name = "empty bar"
                    star1.name = "star foo"
                    star2.name = "star bar"
                    assertNotEquals(empty1.name, empty2.name)
                    assertNotEquals(empty2.name, empty1.name)
                    assertNotEquals(star1.name, star2.name)
                    assertNotEquals(star2.name, star1.name)
                    assertEquals(empty1, empty2)
                    assertEquals(empty2, empty1)
                    assertEquals(star1, star2)
                    assertEquals(star2, star1)
                }
            }
            it("is not equal to a rule with a different replace") {
                for (find in findSample) {
                    val star = starRule(find)
                    val empt = emptyRule(find)
                    assertNotEquals(empt, star)
                    assertNotEquals(star, empt)
                }
            }
            it("is not equal to a rule with a different find") {
                val foo = SimpleRule("ruleName", "foo", "")
                val bar = SimpleRule("ruleName", "bar", "")
                assertNotEquals(foo, bar)
                assertNotEquals(bar, foo)
            }
        }
    }
})
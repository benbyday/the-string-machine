package controller

import model.machinery.Punchcard
import model.machinery.SMOffice
import model.staff.Engineer
import model.staff.Librarian
import model.staff.Secretary
import tornadofx.*

object SMController: Controller() {
    val office = SMOffice
    val punchcardModel = PunchcardModel()
    private val Samuel = Secretary
    private val Eliza = Engineer
    private val Lemon = Librarian

    fun start() {
        Samuel.createOffice("C:\\Users\\ben.polson\\Documents\\Scripts\\kotlin\\string-machine\\src\\main\\"
                + "resources\\office_files")
    }

    fun setMachine(punchcard: Punchcard?) {
        Eliza.setMachine(punchcard)
    }

    fun startMachine(input: String): String {
        return Eliza.startMachine(input)
    }
}
package controller

import model.machinery.Punchcard
import tornadofx.*

class PunchcardModel : ItemViewModel<Punchcard>() {
    val name = bind(Punchcard::nameProperty)
    val instructions = bind(Punchcard::instructionsProperty)
    val broken = bind(Punchcard::brokenProperty)
}
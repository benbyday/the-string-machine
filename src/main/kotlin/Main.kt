import view.SMApp
import javafx.application.Application

/**
 * Start TornadoFX
 */
fun main(args: Array<String>) {
    Application.launch(SMApp::class.java, *args)
}
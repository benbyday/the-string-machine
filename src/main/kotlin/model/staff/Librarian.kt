package model.staff

import model.machinery.*
import model.machinery.SMOffice.ruleLibrary
import model.machinery.SMOffice.ruleCardCatalog
import model.machinery.SMOffice.punchcardFolder

/**
 * Makes sure the [office] is in order. All Boolean tests start with "is". Helps to rename things in the [office] in
 * a way that doesn't break anything. Can add and remove single items in the [office].
 */
object Librarian {
    private val office = SMOffice

    fun addBook(book: RuleBook): Boolean {
        if (!office.addBook(book)) {
            return false
        }
        for (ruleName in book.ruleNames()) {
            office.addReference(RuleReference(book.name, ruleName))
        }
        return true
    }
}
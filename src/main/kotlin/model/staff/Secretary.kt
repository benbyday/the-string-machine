package model.staff

import model.machinery.*
import java.io.File
import java.io.FileNotFoundException

object Secretary {
    private val office = SMOffice

    fun createBook(file: File): RuleBook? {
        val bufferedReader = file.bufferedReader()
        val firstArr = bufferedReader.readLine().split(";")
        if (firstArr[0].toLowerCase() != "name"
                || firstArr[1].toLowerCase() != "find"
                || firstArr[2].toLowerCase() != "replace") {
            return null
        }
        val book = RuleBook(file
                .name
                .replace(Regex("""\.\w+${'$'}"""), "")
                .replace('_', ' ')
                .toLowerCase())
        bufferedReader.forEachLine {
            val array = it.split(";")
            book.add(SimpleRule(
                    array[0]
                            .replace('_', ' ')
                            .toLowerCase(),
                    array[1],
                    array[2]))
        }
        return book
    }

    fun createBook(name: String): RuleBook? {
        return createBook(File(name))
    }

    fun createLibrary(file: File): Boolean {
        var success = true
        Regex("""rule[_\s]*books?""").matchEntire(file.name.toLowerCase())
                ?: throw FileNotFoundException("rule book directory did not match a recognized pattern")
        file.walk().filter { it.isFile }.forEach {
            val book = createBook(it)
            if (book == null
                    || !Librarian.addBook(book)) {
                success = false
            }
        }
        return success
    }

    fun createLibrary(name: String): Boolean {
        return createLibrary(File(name))
    }

    fun createPunchcard(file: File): Punchcard? {
        val bufferedReader = file.bufferedReader()
        val firstArr = bufferedReader.readLine().split(";")
        if (firstArr[0].toLowerCase() != "book"
                || firstArr[1].toLowerCase() != "rule") {
            return null
        }
        val punchcard = Punchcard(file
                .name
                .replace(Regex("""\.\w+${'$'}"""), "")
                .replace('_', ' ')
                .toLowerCase())
        bufferedReader.forEachLine {
            val array = it.split(";")
            val ref = office.getReference(array[0],array[1])
            if (!punchcard.add(ref)) {
                punchcard.broken = true
            }
        }
        return punchcard
    }

    fun createPunchcard(name: String): Punchcard? {
        return createPunchcard(File(name))
    }

    fun createFolder(file: File): Boolean {
        var success = true
        Regex("""punch[_\s]*cards?""").matchEntire(file.name.toLowerCase())
                ?: throw FileNotFoundException("punchcard directory did not match a recognized pattern")
        file.walk().filter { it.isFile }.forEach {
            val punchcard = createPunchcard(it)
            if (punchcard == null
                    || !office.addPunchcard(punchcard)) {
                success = false
            }
        }
        return success
    }

    fun createFolder(name: String): Boolean {
        return createFolder(File(name))
    }

    fun createOffice(file: File): Boolean {
        Regex("""office[_\s]*(?:files)?""").matchEntire(file.name.toLowerCase())
                ?: throw FileNotFoundException("office files directory did not match a recognized pattern")
        val officeDir = file.walk().maxDepth(1).toList()
        return createLibrary(officeDir[2]) && createFolder(officeDir[1])
    }

    fun createOffice(name: String): Boolean {
        return createOffice(File(name))
    }
}
package model.staff

import model.machinery.Punchcard
import model.machinery.StringMachine

object Engineer {
    val machine = StringMachine

    fun setMachine(punchcard: Punchcard?) {
        machine.punchcard = punchcard
    }

    fun startMachine(input: String): String {
        return machine.go(input)
    }
}
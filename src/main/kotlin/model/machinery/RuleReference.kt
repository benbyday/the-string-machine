package model.machinery

import javafx.beans.property.SimpleStringProperty

/**
 * This is a simple class that keeps track of where a particular [rule] is kept in the [SMOffice]. A [RuleReference]
 * also has the ability to change the location of a rule, or rename the location of the rule. The key parts of a
 * [RuleReference] are the [bookName] which is the name of the [RuleBook] in which the [rule] is contained and the
 * other key part is the [ruleName] which is the name of the [Rule].
 *
 * @property bookName The name of the [RuleBook] where the associated [Rule] is located.
 * @property ruleName The name of the associated [Rule].
 * @property rule The actual [Rule] itself.
 */
class RuleReference(_bookName: String, _ruleName: String): Comparable<RuleReference> {
    private val bookNameProperty by lazy { SimpleStringProperty(_bookName) }
    fun bookNameProperty() = bookNameProperty
    var bookName: String
        get() = bookNameProperty.get()
        set(value) {
            SMOffice.renameBook(bookNameProperty.get(), value) ?: throw IllegalArgumentException("Cannot rename a book "
                    + "to an existing book name")
            bookNameProperty.set(value)
        }

    private val ruleNameProperty by lazy { SimpleStringProperty(_ruleName) }
    fun ruleNameProperty() = ruleNameProperty
    var ruleName: String
        get() = ruleNameProperty.get()
        set(value) {
            SMOffice.getBook(bookName)?.rename(ruleNameProperty.get(), value) ?: throw IllegalArgumentException(
                    "Cannot rename a rule to an existing rule name")
            ruleNameProperty.set(value)
        }

    var rule: Rule
        private set

    init {
        rule = grabRule()
    }

    override fun compareTo(other: RuleReference): Int {
        var result = this.bookName.compareTo(other.bookName)
        if (result == 0) {
            result = this.ruleName.compareTo(other.ruleName)
        }
        return result
    }

    override fun toString(): String {
        return "$bookName.$ruleName"
    }

    /**
     * Use the information in this [RuleReference] to find the associated [Rule] in the [SMOffice].
     *
     * @return The associated [Rule] in the [SMOffice].
     */
    private fun grabRule(): Rule {
        SMOffice.getBook(bookName) ?:
                throw IllegalArgumentException("The book \"$bookName\" does not exist in the office ruleLibrary")
        return SMOffice.getBook(bookName)!!.get(ruleName) ?:
                throw IllegalArgumentException("That rule \"$ruleName\" does not exist in the book \"$bookName\"")
    }

    fun refresh() {
        rule = grabRule()
    }

    /**
     * Attempts to take this [rule] and move it to a new [RuleBook] that has the name [newBookName]. If the current
     * [RuleReference] is broken returns null. If a [RuleBook] with the [newBookName] doesn't exist returns null.
     * If a rule with the same signature or name as the current [rule] already exists in the new [RuleBook] returns
     * null. Otherwise on a success returns self.
     *
     * @param newBookName The [RuleBook] where you want to move this [Rule] to in the [SMOffice].
     *
     * @return self after the [Rule] has been moved to a new [RuleBook]. Null upon an unsuccessful move.
     */
    fun changeBook(newBookName: String): RuleReference? {
        SMOffice.getBook(bookName) ?: return null
        val newBook = SMOffice.getBook(newBookName) ?: return null
        newBook.add(rule) ?: return null
        SMOffice.getBook(bookName)!!.remove(rule)
        bookName = newBookName
        return this
    }
}
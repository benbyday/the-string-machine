package model.machinery

import java.lang.IllegalArgumentException

/**
 * The simplest version of a [Rule]. The signature of a [SimpleRule] is defined by the Strings: [find] and [replace].
 * This instance of a [Rule] implements the [implement] function by searching the input string, finding all regex as
 * defined by [find] in the input and replaces them with the [replace] String. On construction it will throw an
 * [IllegalArgumentException] if [find] is empty.
 *
 * @property find This is ultimately a [Regex] to represent the pattern you want to find.
 * @property replace This is the [String] that will replace the pattern that is found.
 */
class SimpleRule(val find: String, val replace: String) : Rule() {
    private val _find: Regex = Regex(find)

    init {
        if (find.isEmpty()) {
            throw IllegalArgumentException("Cannot create a SimpleRule with an empty Regex")
        }
    }

    constructor(name: String, findPattern: String, replace: String) : this(findPattern, replace) {
        this.name = name
    }

    override fun equals(other: Any?): Boolean {
        return when (other) {
            is SimpleRule -> this === other || (this.find == other.find && this.replace == other.replace)
            else -> super.equals(other)
        }
    }

    override fun hashCode(): Int {
        return find.hashCode() + replace.hashCode()
    }

    /**
     * Takes the [input] finds all instances of [_find]. Please note that if there is a sequence where there are
     * overlapping matches of [_find] that as I understand it, the algorithm defaults to match and replace the first
     * instance and ignore all matches that would overlap with that first match and then move on.
     *
     * @param input The [String] that you want to change using this [SimpleRule].
     *
     * @return The changed [String]
     */
    override fun implement(input: String): String {
        return input.replace(_find, replace)
    }

    fun copy(): SimpleRule {
        return SimpleRule(find, replace)
    }
}
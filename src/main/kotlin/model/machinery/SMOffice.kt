package model.machinery

import tornadofx.*

/**
 * This is an object that will hold all of the information regarding [Rule]s and [Punchcard]s.
 *
 * @property ruleLibrary A [List] of all [RuleBook]s.
 * @property ruleCardCatalog A [List] of [RuleReference]s each of which refers to the [Rule]s in the [ruleLibrary].
 * @property punchcardFolder A [List] of all [Punchcard]s.
 */
object SMOffice {
    val ruleLibrary = mutableListOf<RuleBook>().observable()
    val ruleCardCatalog = mutableListOf<RuleReference>().observable()
    val punchcardFolder = mutableListOf<Punchcard>().observable()

    /*
    * Getters
    */

    /**
     * Search the [ruleLibrary] using a binary search since most of the time the [ruleLibrary] should be sorted by the
     * name of the [RuleBook]s.
     *
     * @param bookName The name to search for in the [ruleLibrary].
     * @param fromIndex The index from where you can start searching the [ruleLibrary] inclusive.
     * @param toIndex The index from where you can stop searching in the [ruleLibrary] exclusive.
     *
     * @return The index according to the same rules as [List.binarySearchBy]
     */
    private fun libBinarySearch(bookName: String, fromIndex: Int = 0, toIndex: Int = ruleLibrary.size): Int {
        return ruleLibrary.binarySearchBy(bookName, fromIndex, toIndex) { it.name }
    }

    /**
     * Search the [ruleLibrary] using a binary search since most of the time the [ruleLibrary] should be sorted by the
     * name of the [RuleBook]s.
     *
     * @param book The actual [RuleBook] to search for by name in the [ruleLibrary].
     * @param fromIndex The index from where you can start searching the [ruleLibrary] inclusive.
     * @param toIndex The index from where you can stop searching in the [ruleLibrary] exclusive.
     *
     * @return The index according to the same rules as [List.binarySearchBy]
     */
    private fun libBinarySearch(book: RuleBook, fromIndex: Int = 0, toIndex: Int = ruleLibrary.size): Int {
        return libBinarySearch(book.name, fromIndex, toIndex)
    }

    /**
     * Search the [ruleCardCatalog] using a binary search since most of the time the [ruleCardCatalog] should be sorted
     * by the name of the [Rule]s.
     *
     * @param rule The [Rule] to search for by name in the [ruleCardCatalog].
     * @param fromIndex The index from where you can start searching the [ruleCardCatalog] inclusive.
     * @param toIndex The index from where you can stop searching in the [ruleCardCatalog] exclusive.
     *
     * @return The index according to the same rules as [List.binarySearchBy]
     */
    private fun catBinarySearch(rule: String, fromIndex: Int = 0, toIndex: Int = ruleCardCatalog.size): Int {
        return ruleCardCatalog.binarySearchBy(rule, fromIndex, toIndex) { it.toString() }
    }

    /**
     * Search the [ruleCardCatalog] using a binary search since most of the time the [ruleCardCatalog] should be sorted
     * by the name of the [Rule]s.
     *
     * @param ref The [RuleReference] to search for by the name of the associated [Rule] in the [ruleCardCatalog].
     * @param fromIndex The index from where you can start searching the [ruleCardCatalog] inclusive.
     * @param toIndex The index from where you can stop searching in the [ruleCardCatalog] exclusive.
     *
     * @return The index according to the same rules as [List.binarySearchBy]
     */
    private fun catBinarySearch(ref: RuleReference, fromIndex: Int = 0, toIndex: Int = ruleCardCatalog.size): Int {
        return catBinarySearch(ref.toString(), fromIndex, toIndex)
    }

    /**
     * Search the [punchcardFolder] using a binary search since most of the time the [punchcardFolder] should be sorted
     * by the name of the [Punchcard].
     *
     * @param punchcardName The name of the [Punchcard] you want to search for in the [punchcardFolder].
     * @param fromIndex The index from where you can start searching the [punchcardFolder] inclusive.
     * @param toIndex The index from where you can stop searching in the [punchcardFolder] exclusive.
     *
     * @return The index according to the same rules as [List.binarySearchBy]
     */
    private fun folBinarySearch(punchcardName: String, fromIndex: Int = 0, toIndex: Int = punchcardFolder.size): Int {
        return punchcardFolder.binarySearchBy(punchcardName, fromIndex, toIndex) { it.name }
    }

    /**
     * Search the [punchcardFolder] using a binary search since most of the time the [punchcardFolder] should be sorted
     * by the name of the [Punchcard].
     *
     * @param punchcard The actual [Punchcard] you want to search for in the [punchcardFolder].
     * @param fromIndex The index from where you can start searching the [punchcardFolder] inclusive.
     * @param toIndex The index from where you can stop searching in the [punchcardFolder] exclusive.
     *
     * @return The index according to the same rules as [List.binarySearchBy]
     */
    private fun folBinarySearch(punchcard: Punchcard, fromIndex: Int = 0, toIndex: Int = punchcardFolder.size): Int {
        return folBinarySearch(punchcard.name, fromIndex, toIndex)
    }

    /**
     * returns the [RuleBook] from the [ruleLibrary] that has the provided [bookName].
     *
     * @param bookName The name of the book you want to find.
     *
     * @return The [RuleBook] from the [ruleLibrary] with the given [bookName] or null if not found.
     */
    fun getBook(bookName: String): RuleBook? {
        val i = libBinarySearch(bookName)
        return if (i < 0) {
            null
        } else {
            ruleLibrary[i]
        }
    }

    /**
     * Get the [Rule] in the [ruleLibrary] according to the [ruleName] and what [RuleBook] that [Rule] can be found in.
     *
     * @param bookName The book wherein you can find the corresponding [Rule].
     * @param ruleName The name of the [Rule]
     *
     * @return The [Rule] with the given [ruleName] in the [RuleBook] with the given [bookName] or null if not found.
     */
    fun getRule(bookName: String, ruleName: String): Rule? {
        return getBook(bookName)?.get(ruleName)
    }

    /**
     * Get the [Rule] in the [ruleLibrary] by [RuleReference].
     *
     * @param ref The [RuleReference] to utilize how to find the [Rule] in the [ruleLibrary]
     *
     * @return The [Rule] in the [ruleLibrary] corresponding to the [RuleReference] or null if not found.
     */
    fun getRule(ref: RuleReference): Rule? {
        return getRule(ref.bookName, ref.ruleName)
    }

    /**
     * Find the [RuleReference] in the [ruleCardCatalog] according to the [bookName] and [ruleName].
     *
     * @param bookName The name of the corresponding [RuleBook].
     * @param ruleName The name of the corresponding [Rule].
     *
     * @return The [RuleReference] referring to the [Rule] with the given [ruleName] in the [RuleBook] with the given
     * [bookName]. Returns null if there is no such [RuleBook], no such [Rule], and no such [RuleReference] in the
     * [ruleCardCatalog].
     */
    fun getReference(bookName: String, ruleName: String): RuleReference? {
        val i = catBinarySearch("$bookName.$ruleName")
        return if (i < 0) {
            null
        } else {
            ruleCardCatalog[i]
        }
    }

    /**
     * Get the [Punchcard] with the specified [punchcardName] or null if it can't be found.
     *
     * @param punchcardName The name of the [Punchcard] to find.
     *
     * @return The corrosponding [Punchcard] or null if not found.
     */
    fun getPunchcard(punchcardName: String): Punchcard? {
        val i = folBinarySearch(punchcardName)
        return if (i < 0) {
            null
        } else {
            punchcardFolder[i]
        }
    }

    /**
     * Get a [List] of all [Punchcard]s where the given [RuleReference] appears somewhere on the [Punchcard].
     *
     * @param reference The [RuleReference] to search for on each [Punchcard] in the [punchcardFolder].
     *
     * @return A [List] of [Punchcard]s.
     */
    fun getPunchcardList(reference: RuleReference): List<Punchcard> {
        return punchcardFolder.filter {
            it.contains(reference)
        }
    }

    /**
     * Get a [List] of all [Punchcard]s where the [RuleReference] corresponding to the given [bookName] and [ruleName]
     * appears somewhere on the [Punchcard].
     *
     * @param bookName one of the components of the [RuleReference] needed.
     * @param ruleName one of the components of the [RuleReference] needed.
     *
     * @return A [List] of [Punchcard]s.
     */
    fun getPunchcardList(bookName: String, ruleName: String): List<Punchcard> {
        val ref = getReference(bookName, ruleName) ?: return listOf()
        return getPunchcardList(ref)
    }

    /*
    * Add Methods
    */

    /**
     * Adds a [newBook] to the [ruleLibrary]. If there is already a book with the same name in the [ruleLibrary] then
     * returns null.
     *
     * @param newBook The book you want to add
     *
     * @return True upon a successful add, false if a [RuleBook] of the same name already exists in the [ruleLibrary].
     */
    fun addBook(newBook: RuleBook): Boolean {
        val i = libBinarySearch(newBook)
        return if (i >= 0) {
            false
        } else {
            ruleLibrary.add(-(i + 1), newBook)
            true
        }
    }

    /**
     * Adds the [punchcard] to the [punchcardFolder]. Returns false if there is already a punchcard with the same name
     * in the [punchcardFolder].
     *
     * @param punchcard The punchcard you want to add.
     *
     * @return True upon a successful add. False if a [Punchcard] with the same name already exists in the
     * [punchcardFolder]
     */
    fun addPunchcard(punchcard: Punchcard): Boolean {
        val i = folBinarySearch(punchcard)
        return if (i >= 0) {
            false
        } else {
            punchcardFolder.add(-(i + 1), punchcard)
            true
        }
    }

    /**
     * Add a [RuleReference] to the [ruleCardCatalog]. Returns false if the [reference] already exists in the
     * [ruleCardCatalog].
     *
     * @param reference The [RuleReference] you want to add to the [ruleCardCatalog].
     *
     * @return True if adding the [RuleReference] is successful. False if the [RuleReference] already exists in the
     * [ruleCardCatalog].
     */
    fun addReference(reference: RuleReference): Boolean {
        val i = catBinarySearch(reference)
        return if (i >= 0) {
            false
        } else {
            ruleCardCatalog.add(-(i + 1), reference)
            true
        }
    }

    /*
    * Remove Methods
    */

    /**
     * removes the [rule][ref] from the [ruleLibrary].
     *
     * @param ref The [RuleReference] corresponding to the [Rule] you want to remove from which [RuleBook] from the
     * [ruleLibrary].
     *
     * @return The rule that got removed or else null if no such rule could be found.
     */
    fun removeRule(ref: RuleReference): Rule? {
        val book = getBook(ref.bookName) ?: return null
        val rule = book[ref.ruleName] ?: return null
        book.remove(rule)
        return rule
    }

    /**
     * Removes the book with the specified [name] from the [ruleLibrary]. If no such [RuleBook] exists, returns null.
     *
     * @param name the name of the [RuleBook] you want to remove.
     *
     * @return The [RuleBook] you have removed from the [ruleLibrary] if it was found. Else null.
     */
    fun removeBook(name: String): RuleBook? {
        val i = libBinarySearch(name)
        return if (i < 0) {
            null
        } else {
            ruleLibrary.removeAt(i)
        }
    }

    /**
     * Removes the [punchcard][name] from the [punchcardFolder].
     *
     * @param name The name of the [Punchcard] you want to remove.
     *
     * @return The [Punchcard] you have removed from the [punchcardFolder] if it was found. Else null.
     */
    fun removePunchcard(name: String): Punchcard? {
        val i = folBinarySearch(name)
        return if (i < 0) {
            null
        } else {
            punchcardFolder.removeAt(i)
        }
    }

    /**
     * Removes the specified [RuleReference] from the [ruleCardCatalog].
     *
     * @param ref the [RuleReference] you want to remove from the [ruleCardCatalog].
     *
     * @return True if the removal was successful and the [RuleReference] was found. False if it was not found.
     */
    fun removeReference(ref: RuleReference): Boolean {
        val i = catBinarySearch(ref)
        return if (i < 0) {
            false
        } else {
            ruleCardCatalog.removeAt(i)
            true
        }
    }

    /*
    * Edit Methods
    */

    /**
     * finds a book in the [ruleLibrary] with the [oldName] and changes the name of the [RuleBook] to the [newName]. If
     * there is no such [RuleBook] with the [oldName] the method returns null. If there is already a [RuleBook] in the
     * [ruleLibrary] with the [newName] then the renaming doesn't happen and the method returns null.
     *
     * @param oldName the name of the [RuleBook] you want to change.
     * @param newName the new name you want to give to the [RuleBook] found.
     *
     * @return The [RuleBook] upon a successful renaming. Else, null.
     */
    fun renameBook(oldName: String, newName: String): RuleBook? {
        val book = getBook(oldName) ?: return null
        if (oldName == newName) {
            return book
        }
        if (getBook(newName) != null) {
            return null
        }
        removeBook(oldName)
        book.name = newName
        addBook(book)
        return book
    }

    /**
     * changes the name of a [Punchcard] from the [punchcardFolder] from the [oldName] to the [newName].
     *
     * @param oldName The name of the [Punchcard] you want to change.
     * @param newName The new name you want to give the [Punchcard] that is found.
     *
     * @return The [Punchcard] upon a successful renaming. Else, null.
     */
    fun renamePunchcard(oldName: String, newName: String): Punchcard? {
        val punchcard = getPunchcard(oldName) ?: return null
        if (oldName == newName) {
            return punchcard
        }
        if (getPunchcard(newName) != null) {
            return null
        }
        removePunchcard(oldName)
        punchcard.name = newName
        addPunchcard(punchcard)
        return punchcard
    }

    /**
     * goes through the [punchcardFolder] and any time a [Punchcard] has an instruction with an [oldRef] it replaces it
     * with the [newRef].
     *
     * @param oldRef When this [RuleReference] is found on a [Punchcard] it will be replaced.
     * @param newRef This is the [RuleReference] that will replace the [oldRef] on [Punchcard]s.
     */
    fun replaceInstruction(oldRef: RuleReference, newRef: RuleReference) {
        for (punchcard in getPunchcardList(oldRef)) {
            punchcard.replace(oldRef, newRef)
        }
    }
}
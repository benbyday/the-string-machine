package model.machinery

/**
 * The String Machine takes an input, applies all of the rules on a [punchcard] to that input and gives back the
 * result.
 *
 * @property punchcard This is the current mode that the [StringMachine] is set to. It holds the list of rules that
 * will be applied to an input.
 */
object StringMachine {
    var punchcard: Punchcard? = null

    /**
     * Run the string machine. Apply the [punchcard] to the [input].
     *
     * @param input The input to apply all of the [Rule]s on the [punchcard]
     *
     * @return the result of applying all of the rules on the [punchcard] to the [input].
     */
    fun go(input: String): String {
        var string = input
        if (punchcard == null) {
            throw RuntimeException("You must select a punchcard for the String Machine")
        }
        for (instruction in punchcard!!.instructions) {
            string = instruction.rule.implement(string)
        }
        return string
    }
}
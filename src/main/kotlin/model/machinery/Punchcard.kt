package model.machinery

import javafx.beans.property.SimpleListProperty
import javafx.collections.ObservableList
import tornadofx.*

/**
 * A [Punchcard] holds a list of [instructions] where each instruction is a [Rule].
 */
class Punchcard(_name: String) {
    var name: String by property(_name)
    fun nameProperty() = getProperty(Punchcard::name)

    private val instructionsProperty by lazy {
        SimpleListProperty<RuleReference>(mutableListOf<RuleReference>().observable())
    }
    fun instructionsProperty() = instructionsProperty
    var instructions: ObservableList<RuleReference>
        get() = instructionsProperty.get()
        set(value) = instructionsProperty.set(value)

    var broken: Boolean by property(false)
    fun brokenProperty() = getProperty(Punchcard::broken)


    /**
     * Checks to make sure both of the integers provided are greater than or equal to 0 and they are both less than the
     * length of the [Punchcard].
     */
    private fun checkBounds(i1: Int, i2: Int): Boolean {
        var ind1 = i1
        var ind2 = i2
        if (ind2 < ind1) {
            val temp = ind1
            ind1 = ind2
            ind2 = temp
        }
        return (ind2 < instructions.size && ind1 >= 0)
    }

    operator fun get(ind: Int): RuleReference {
        return instructions[ind]
    }

    /**
     * returns a non mutable list of the [RuleReference]s.
     */
    fun rules(): List<RuleReference> {
        return instructions
    }

    /**
     * returns the size of the [Punchcard].
     */
    fun size(): Int {
        return instructions.size
    }

    /**
     * adds a list of [RuleReference]s to the [Punchcard]. returns false if something goes wrong.
     */
    fun addAll(rules: Iterable<RuleReference>): Boolean {
        return this.instructions.addAll(rules)
    }

    /**
     * add a single [rule] to the end of the [Punchcard]
     */
    fun add(rule: RuleReference?): Boolean {
        return if (rule == null) {
            false
        } else {
            instructions.add(rule)
        }
    }

    /**
     * adds the specified [rule] to be in the position of the specified [index].
     */
    fun add(index: Int, rule: RuleReference?): Boolean {
        return if (rule == null) {
            false
        } else {
            instructions.add(index,rule)
            true
        }
    }

    /**
     * removes whatever element is located at the specified [index]
     */
    fun removeAt(index: Int): RuleReference {
        return instructions.removeAt(index)
    }

    /**
     * removes every instance of a [reference] from the [instructions]
     */
    fun remove(reference: RuleReference): Int {
        var count = 0
        for (ind in instructions.size - 1 downTo 0) {
            if (instructions[ind] == reference) {
                instructions.removeAt(ind)
                count++
            }
        }
        return count
    }

    override fun toString(): String {
        return name
    }

    /**
     * changes every [oldRef] that is listed in the [instructions] to the [newRef].
     */
    fun replace(oldRef: RuleReference, newRef: RuleReference) {
        for (ind in 0 until instructions.size) {
            if (instructions[ind] == oldRef) {
                instructions[ind] = newRef
            }
        }
    }

    /**
     * clears out the entire [instructions].
     */
    fun clear() {
        instructions.clear()
    }

    /**
     * returns whether or not the [instructions] contain the [reference]
     */
    fun contains(reference: RuleReference): Boolean {
        return instructions.contains(reference)
    }

    /**
     * exchanges the instruction at [ind1] with the one at [ind2].
     */
    fun swap(ind1: Int, ind2: Int): Boolean {
        if (ind1 == ind2) {
            return true
        }
        if (!checkBounds(ind1, ind2)) {
            return false
        }
        val temp = instructions[ind1]
        instructions[ind1] = instructions[ind2]
        instructions[ind2] = temp
        return true
    }

    fun count(instruction: RuleReference): Int {
        return instructions.count { it == instruction }
    }

    /**
     * moves the instruction which is at the location [start] up the list by some [number][move] of spots. Fails if
     * [move] is less than zero.
     */
    fun moveUp(start: Int, move: Int = 1): Boolean {
        if (move < 0) {
            return false
        }
        return move(start, -move)
    }

    /**
     * moves the instruction which is at the location [start] down the list by some [number][move] of spots. Fails if
     * [move] is less than zero.
     */
    fun moveDown(start: Int, move: Int = 1): Boolean {
        if (move < 0) {
            return false
        }
        return move(start, move)
    }

    /**
     * private function that helps to accomplish all move functions.
     */
    private fun move(start: Int, move: Int = 1): Boolean {
        if (move == 0) {
            return true
        }
        val end = start + move
        if (!checkBounds(start, end)) {
            return false
        }
        val rule = instructions.removeAt(start)
        instructions.add(end, rule)
        return true
    }
}
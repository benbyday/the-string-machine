package model.machinery

import tornadofx.*

/**
 * Each RuleBook has a [name] and a list of [rules]. All Rules in a RuleBook must have a unique name and a unique rule
 * associated with them.
 *
 * @property name The name of this [RuleBook].
 * @property rules The list of [Rule]s inside this [RuleBook].
 */
class RuleBook(_name: String) {
    var name: String by property(_name)
    fun nameProperty() = getProperty(RuleBook::name)

    private val rules = mutableListOf<Rule>().observable()
    operator fun get(name: String): Rule? {
        val i = binarySearch(name)
        return if (i < 0) {
            null
        } else {
            rules[i]
        }
    }

    fun binarySearch(name: String, fromIndex: Int = 0, toIndex: Int = rules.size): Int {
        return rules.binarySearchBy(name, fromIndex, toIndex) { it.name }
    }

    fun binarySearch(rule: Rule, fromIndex: Int = 0, toIndex: Int = rules.size): Int {
        return binarySearch(rule.name, fromIndex, toIndex)
    }

    fun ruleNames(): Set<String> {
        return rules.map { it.name }.toSet()
    }

    /**
     * adds a [newRule] to the [rules]. The [newRule] must have a unique name and a unique rule that is not yet in this
     * [RuleBook]. Otherwise the method returns null.
     */
    fun add(newRule: Rule): Rule? {
        val i = binarySearch(newRule)
        if (i >= 0) {
            return null
        }
        for (rule in rules) {
            if (rule == newRule) {
                return null
            }
        }
        rules.add(-(i + 1), newRule)
        return newRule
    }

    /**
     * removes the specified [rule] from the [rules] in this [RuleBook]. Determines which rule to remove based on the
     * find and replace values of the specified [rule] not necessarily on the name of the rule. If the remove was
     * successful returns true. Otherwise returns false.
     */
    fun remove(rule: Rule): Boolean {
        return rules.remove(rule)
    }

    /**
     * returns the number of rules contained in this [RuleBook].
     */
    fun size(): Int {
        return rules.size
    }

    fun sort() {
        rules.sortBy { it.name }
    }

    /**
     * Searches the [rules] for a rule that has the same signature as the [rule][findRule] given. Remember the
     * signature is based on the find and replace values, not the name. If no such rule is found returns null.
     */
    fun findRuleName(findRule: Rule): String? {
        for (rule in rules) {
            if (rule == findRule) {
                return rule.name
            }
        }
        return null
    }

    /**
     * Searches the [rules] for a rule with the [oldName] and renames that rule with the [newName]. If there is no such
     * rule with the [oldName] returns null. If there is already a rule in [rules] with the [newName] then the rule is
     * not renamed and the method returns null.
     */
    fun rename(oldName: String, newName: String): Rule? {
        val oldInd = binarySearch(oldName)
        if (oldInd < 0) return null
        val rule = rules[oldInd]
        val newInd = binarySearch(newName)
        if (newInd >= 0) {
            return null
        }
        rules.removeAt(oldInd)
        rule.name = newName
        rules.add(newInd, rule)
        return rule
    }
}
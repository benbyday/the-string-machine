package model.machinery

import tornadofx.*

/**
 * Each rule has a [name] and a function that takes in a String, [implement]s some change to the string and returns the
 * result.
 */
abstract class Rule {
    var name: String by property("")
    fun nameProperty() = getProperty(Rule::name)
    abstract fun implement(input: String): String
}
package view

import controller.SMController
import javafx.geometry.Pos
import javafx.scene.control.TextArea
import javafx.scene.text.Font
import javafx.scene.text.TextAlignment
import model.machinery.Punchcard
import model.machinery.RuleReference
import tornadofx.*

class SMView: View() {
    private val controller = SMController
    private var input: TextArea by singleAssign()
    private var output: TextArea by singleAssign()

    override val root = borderpane {
        top {
            val title = label("String Machine") {
                font = Font("Algerian", 48.0)
                textAlignment = TextAlignment.CENTER
                alignment = Pos.TOP_CENTER
            }
            title.minWidthProperty().bind(this.widthProperty())
        }
        center {
            vbox {
                label("Enter the string you want to manipulate in the machine:")
                input = textarea {
                    isWrapText = true
                }
                button("Start Machine") {
                    alignment = Pos.TOP_CENTER
                    setOnAction {
                        output.text = controller.startMachine(input.text)
                    }
                }
                label("Here is where the manipulated string will show up:")
                output = textarea {
                    isEditable = false
                    isWrapText = true
                }
                alignment = Pos.TOP_CENTER
                spacing = 5.0
                paddingLeft = 5.0
                paddingRight = 5.0
                paddingBottom = 5.0
            }
        }
        left {
            vbox {
                combobox<Punchcard> {
                    items = controller.office.punchcardFolder
                    alignment = Pos.TOP_CENTER
                    setOnAction {
                        controller.punchcardModel.rebind {
                            item = selectedItem
                        }
                        controller.setMachine(selectedItem)
                    }
                }
                listview<RuleReference>(controller.punchcardModel.instructions)
                spacing = 5.0
                paddingLeft = 5.0
                paddingBottom = 5.0
            }
        }
        minWidth = 500.0
    }
}
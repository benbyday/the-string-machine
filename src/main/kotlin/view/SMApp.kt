package view

import controller.SMController
import tornadofx.App

class SMApp: App(SMView::class) {
    init {
        SMController.start()
    }
}